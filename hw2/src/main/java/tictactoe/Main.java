package tictactoe;

import tictactoe.board.TicTacToeBoard;
import tictactoe.player.HumanPlayer;
import tictactoe.player.MinimaxPlayer;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        final int n, m;

        System.out.println("Enter dimensions (n and m) for board:");
        try (Scanner in = new Scanner(System.in)) {
            String line = in.nextLine();

            try {
                Scanner scanner = new Scanner(line);
                n = scanner.nextInt();
                m = scanner.nextInt();

                if (scanner.hasNext() || n <= 0 || m <= 0) {
                    System.out.println("Wrong input!");
                } else {
                    final Game game = new Game(true, new HumanPlayer(), new MinimaxPlayer(n, m));
                    System.out.println("Game result: " + game.play(new TicTacToeBoard(n, m, 2)));
                }
            } catch (InputMismatchException e) {
                System.out.println("Wrong input!");
            }
        } catch (InputMismatchException e) {
            System.out.println("Wrong input!");
        }
    }
}
