package tictactoe;

import tictactoe.board.Board;
import tictactoe.player.Player;

public class Game {
    private final Player firstPlayer, secondPlayer;
    private final boolean isLogging;

    public Game(final boolean isLogging, final Player firstPlayer, final Player secondPlayer) {
        this.firstPlayer = firstPlayer;
        this.secondPlayer = secondPlayer;
        this.isLogging = isLogging;

        if (firstPlayer == null) {
            throw new RuntimeException("Invalid first player");
        } else if (secondPlayer == null) {
            throw new RuntimeException("Invalid second player");
        }
    }

    public int play(Board board) {
    	Player[] players = {firstPlayer, secondPlayer};
        int playerNumber = 1;

        log("Position:\n" + board, false);

        while (true) {
            final int result = move(board, players[playerNumber - 1], playerNumber);
            if (result > -1) {
                return result;
            }
            playerNumber = (playerNumber % 2) + 1;
        }
    }

    private int move(final Board board, final Player player, final int no) {
        final Move move = player.move(board.getPosition(), board.getTurn());
        final GameResult result = board.makeMove(move);

        log("Player " + no + " move: " + move, isLogging);
        log("Position:\n" + board, false);
        
        if (result == GameResult.WIN) {
            log("Player " + no + " won", false);
            return no;
        } else if (result == GameResult.DRAW) {
            log("Draw", false);
            return 0;
        } else {
        	return -1;
        }
    }

    private void log(final String message, final boolean check) {
        if (!check || isLogging) {
            System.out.println(message);
        }
    }
}
