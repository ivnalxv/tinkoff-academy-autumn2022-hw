package tictactoe;

public enum GameResult {
    WIN, LOSE, DRAW, UNKNOWN
}
