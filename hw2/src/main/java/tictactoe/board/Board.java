package tictactoe.board;

import tictactoe.Cell;
import tictactoe.Move;
import tictactoe.GameResult;

public interface Board {
    Cell getTurn();
    void setTurn(Cell turn);
    int getEmptyCells();
    void setEmptyCells(int newEmptyCells);
    GameResult makeMove(Move move);
    Position getPosition();
    Cell[][] getCells();
}
