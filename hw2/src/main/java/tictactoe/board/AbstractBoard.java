package tictactoe.board;

import tictactoe.Cell;
import tictactoe.Move;
import tictactoe.GameResult;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

public abstract class AbstractBoard implements Board {
	protected static final Map<Cell, Character> SYMBOLS = Map.of(
            Cell.X, 'X',
            Cell.O, 'O',
            Cell.E, '.',
            Cell.N, ' '
    );


    protected final Cell[][] cells;
    protected final int height, width, winningLength;
    protected int emptyCells;
    protected Cell turn;

    public AbstractBoard(int height, int width, int winningLength) {
        this(new Cell[height][width], Cell.X, height * width, winningLength);
        for (Cell[] row : cells) {
            Arrays.fill(row, Cell.E);
        }
    }

    public AbstractBoard(Cell[][] cells, Cell turn, int emptyCells, int winningLength) {
        this.height = cells.length;
        this.width = cells[0].length;
        this.winningLength = winningLength;
        this.turn = turn;
        this.emptyCells = emptyCells;
        this.cells = cells;
    }

    protected boolean isPlayerCell(int row, int column, Cell cell) {
        return 0 <= row && row < height
                && 0 <= column && column < width
                && turn != Cell.N
                && cells[row][column] == cell;
    }

    private int maxCellLine(int row, int column) {
        int[][] directions = { {0, 1}, {1, 0}, {1, 1}, {1, -1} };
        int max = 0;
        for (int[] dir: directions) {
            max = Math.max(max, maxLineByDirection(row, column, dir[0], dir[1])
                    + maxLineByDirection(row, column, -dir[0], -dir[1]));
        }
        return max + 1;
    }

    private int maxLineByDirection(int row, int column, int i, int j) {
        int len = 0;
        while (isPlayerCell(row + i * (len + 1), column + j * (len + 1), cells[row][column])) {
            len++;
        }
        return len;
    }

    protected GameResult getGameResult(int row, int column) {
        int maxCellLine =  maxCellLine(row, column);

        if (maxCellLine >= winningLength) {
            return GameResult.WIN;
        } else if (emptyCells == 0) {
            return GameResult.DRAW;
        } else {
            return GameResult.UNKNOWN;

        }
    }

    @Override
    public GameResult makeMove(final Move move) {
        if (!isValidMove(move)) {
            return GameResult.LOSE;
        }

        int row = move.getRow();
        int column = move.getColumn();
        cells[row][column] = move.getValue();
        emptyCells--;
        turn = turn == Cell.X ? Cell.O : Cell.X;

        return getGameResult(row, column);
    }

    public Position getPosition() {
        return new Position() {
            @Override
            public Cell getCell(int row, int column) {
                return AbstractBoard.this.cells[row][column];
            }

            @Override
            public boolean isValidMove(Move move) {
                return AbstractBoard.this.isValidMove(move);
            }

            @Override
            public int getHeight() {
                return AbstractBoard.this.height;
            }

            @Override
            public int getWidth() {
                return AbstractBoard.this.width;
            }

            @Override
            public int getWinningLength() {
                return AbstractBoard.this.winningLength;
            }

            @Override
            public String toString() {
                return AbstractBoard.this.toString();
            }

            @Override
            public int hashCode() {
                return AbstractBoard.this.hashCode();
            }
        };
    }

    @Override
    public Cell getTurn() {
        return turn;
    }

    @Override
    public void setTurn(Cell turn) {
        this.turn = turn;
    }

    @Override
    public void setEmptyCells(int newEmptyCells) {
        emptyCells = newEmptyCells;
    }

    @Override
    public int getEmptyCells() {
        return emptyCells;
    }


    @Override
    public Cell[][] getCells() {
        return cells;
    }

    public boolean isValidMove(final Move move) {
        if (move == null) {
            throw new RuntimeException("Impossible move");
        }

        return 0 <= move.getRow() && move.getRow() < height
                && 0 <= move.getColumn() && move.getColumn() < width
                && cells[move.getRow()][move.getColumn()] == Cell.E
                && turn == getTurn();
    }

    private int numberLength(int num) {
        return num == 0 ? 1 : ((int) Math.floor(Math.log10(num)) + 1);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        final int boxSize = numberLength(width) + 1;

        sb.append(" ".repeat(Math.max(0, boxSize)));
        for (int c = 0; c < width; c++) {
            sb.append(" ".repeat(Math.max(0, boxSize - numberLength(c))));
        	sb.append(c);
        }
        
        for (int r = 0; r < height; r++) {
            sb.append("\n");
            sb.append(" ".repeat(Math.max(0, boxSize - numberLength(r))));
            sb.append(r);
            for (int c = 0; c < width; c++) {
                sb.append(" ".repeat(Math.max(0, boxSize - 1)));
                sb.append(SYMBOLS.get(cells[r][c]));
            }
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractBoard that = (AbstractBoard) o;
        return height == that.height && width == that.width && winningLength == that.winningLength
                && emptyCells == that.emptyCells
                && Arrays.deepEquals(cells, that.cells);
    }

    @Override
    public int hashCode() {
        return Objects.hash(height, width, winningLength, turn, emptyCells, Arrays.deepHashCode(cells));
    }
}
