package tictactoe.board;

import tictactoe.Cell;

import java.util.Arrays;

public class TicTacToeBoard extends AbstractBoard {
    public TicTacToeBoard(int n) {
    	this(n, n, Math.min(3, n));
    }

    public TicTacToeBoard(int n, int m, int k) {
        super(n, m, k);
        emptyCells = n * m;
        for (Cell[] row : cells) {
            Arrays.fill(row, Cell.E);
        }
    }

    public TicTacToeBoard(Cell[][] cells, Cell turn, int emptyCells, int k) {
        super(cells, turn, emptyCells, k);
    }
}
