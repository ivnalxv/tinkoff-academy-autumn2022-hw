package tictactoe.board;

import tictactoe.Cell;
import tictactoe.Move;

public interface Position {
	Cell getCell(int row, int column);
	boolean isValidMove(Move move);
	int getHeight();
	int getWidth();
	int getWinningLength();
}
