package tictactoe.player;

import tictactoe.Cell;
import tictactoe.Move;
import tictactoe.board.Position;

import java.util.Random;


public class RandomPlayer implements Player {
    private final Random random;

    public RandomPlayer(final Random random) {
        this.random = random;
    }

    public RandomPlayer() {
        this(new Random());
    }

    @Override
    public Move move(final Position position, final Cell cell) {
        while (true) {
            int r = random.nextInt(position.getHeight());
            int c = random.nextInt(position.getWidth());
            final Move move = new Move(r, c, cell);
            if (position.isValidMove(move)) {
                return move;
            }
        }
    }
}
