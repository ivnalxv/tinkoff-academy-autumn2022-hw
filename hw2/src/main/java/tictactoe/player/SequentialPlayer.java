package tictactoe.player;

import tictactoe.Cell;
import tictactoe.Move;
import tictactoe.board.Position;

public class SequentialPlayer implements Player {
	@Override
    public Move move(final Position position, final Cell cell) {
		final int n = position.getHeight();
		final int m = position.getWidth();
		
        for (int r = 0; r < n; r++) {
            for (int c = 0; c < m; c++) {
                final Move move = new Move(r, c, cell);
                if (position.isValidMove(move)) {
                    return move;
                }
            }
        }
        throw new IllegalStateException("No valid moves");
    }
}
