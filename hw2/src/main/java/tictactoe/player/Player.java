package tictactoe.player;

import tictactoe.Cell;
import tictactoe.Move;
import tictactoe.board.Position;

public interface Player {
    Move move(Position position, Cell cell);
}
