package tictactoe.player;

import tictactoe.Cell;
import tictactoe.Move;
import tictactoe.board.Position;
import tictactoe.board.TicTacToeBoard;

import java.util.HashMap;
import java.util.Map;

public class MinimaxPlayer implements Player {
    private final Map<Integer, Move> boardToMove;
    private final Map<Integer, Integer> boardToScore;
    private final int n, m;

    // Currently supports only two players
    public MinimaxPlayer(int n, int m) {
        this.n = n;
        this.m = m;
        boardToMove = new HashMap<>();
        boardToScore = new HashMap<>();
        calculateDecisionTree(new TicTacToeBoard(n, m, 3));
    }

    private int calculateDecisionTree(TicTacToeBoard board) {
        if (boardToScore.containsKey(board.hashCode())) {
            return boardToScore.get(board.hashCode());
        }

        boolean isTurnX = board.getTurn() == Cell.X;
        int optimalScore = isTurnX ? Integer.MIN_VALUE : Integer.MAX_VALUE;

        Move nextMove = null;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                Move move = new Move(i, j, board.getTurn());
                if (!board.isValidMove(move)) continue;


                int score = switch (board.makeMove(move)) {
                    case UNKNOWN -> calculateDecisionTree(board);
                    case WIN -> isTurnX ? 1 : -1;
                    case LOSE -> isTurnX ? -1 : 1;
                    case DRAW -> 0;
                };

                board.getCells()[i][j] = Cell.E;
                board.setTurn(move.getValue());
                board.setEmptyCells(board.getEmptyCells() + 1);


                if ((isTurnX && score > optimalScore) || (!isTurnX && score < optimalScore)) {
                    optimalScore = score;
                    nextMove = move;
                }
            }
        }

        if (nextMove == null) {
            throw new RuntimeException("Impossible prediction in minimax: " + board);
        }

        boardToMove.put(board.hashCode(), nextMove);
        boardToScore.put(board.hashCode(), optimalScore);
        return optimalScore;
    }

    @Override
    public Move move(Position position, Cell cell) {
        Move move = boardToMove.get(position.hashCode());
        if (move == null) {
            throw new RuntimeException("Impossible move in minimax: " + position);
        }
        return move;
    }
}
