package tictactoe.player;

import tictactoe.Cell;
import tictactoe.Move;
import tictactoe.board.Position;

import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;


public class HumanPlayer implements Player {
    private final PrintStream out;
    private final Scanner in;

    public HumanPlayer(final PrintStream out, final Scanner in) {
        this.out = out;
        this.in = in;
    }

    public HumanPlayer() {
        this(System.out, new Scanner(System.in));
    }

    @Override
    public Move move(final Position position, final Cell cell) {
        while (true) {
            out.println("Enter row and column");
            
            int row, column;
            try {
            	String line = in.nextLine();

                try (Scanner scanner = new Scanner(line)) {
                    row = scanner.nextInt();
                    column = scanner.nextInt();

                    final Move move = new Move(row, column, cell);
                    if (position.isValidMove(move)) {
                        return move;
                    }
                    out.println("Move " + move + " is invalid");
                } catch (InputMismatchException e) {
                    out.println("Invalid input. Try again.");
                }
            } catch (NoSuchElementException e) {
                throw new NoSuchElementException("No such element.");
            } catch (IllegalStateException e) {
                throw new IllegalStateException("Human scanner is closed!");
            }
        }
    }
}
