package java.task04;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.Executors;

public class ConcurrentTradeOrganisation {
    private static final int SHUTDOWN_TIMEOUT = 6000;
    private static final int SUPPLYING_TIMEOUT = 200;
    private static final int CONSUMING_TIMEOUT = 300;
    private static final Random random = new Random();
    private ExecutorService service;

    private static Runnable createSupplier(BlockingQueue<String> objectQueue, int id) {
        return () -> {
            try {
                while (!Thread.interrupted()) {
                    String object = Integer.toString(random.nextInt(100));
                    TimeUnit.MILLISECONDS.sleep(SUPPLYING_TIMEOUT);
                    objectQueue.put(object);
                    System.out.println("[INFO] Object: " + object + " was supplied by " + id);
                }
            } catch (InterruptedException ignored) {
                //  intentionally empty
            }
            System.out.println("[INFO] Supplier " + id + " is down");
        };
    }

    private static Runnable createConsumer(BlockingQueue<String> objectQueue, int id) {
        return () -> {
            try {
                while (!Thread.interrupted()) {
                    if (!objectQueue.isEmpty()) {
                        TimeUnit.MILLISECONDS.sleep(CONSUMING_TIMEOUT);
                        System.out.println("[INFO] Object: " + objectQueue.poll() + " was consumed by " + id);
                    }
                }
            } catch (InterruptedException ignored) {
                //  intentionally empty
            }
            System.out.println("[INFO] Consumer " + id + " is down");
            Thread.currentThread().interrupt();
        };
    }

    public void start(int supplierNumber, int consumerNumber) {
        BlockingQueue<String> objectQueue = new LinkedBlockingQueue<>();
        service = Executors.newCachedThreadPool();

        for (int i = 0; i < supplierNumber; i++) {
            service.execute(createSupplier(objectQueue, i));
        }

        for (int i = 0; i < consumerNumber; i++) {
            service.execute(createConsumer(objectQueue, i));
        }

        waitBeforeShutdown(SHUTDOWN_TIMEOUT);
    }

    public void waitBeforeShutdown(long waitInMilliSeconds) {
        try {
            if (!service.awaitTermination(waitInMilliSeconds, TimeUnit.MILLISECONDS)) {
                System.err.println("[INFO] Waiting time run out.");
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted.", e);
        } finally {
            service.shutdownNow();
        }
    }
}
