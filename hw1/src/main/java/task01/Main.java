package java.task01;

import java.util.List;

public class Main {
    public static List<Integer> firstTask(List<Integer> list) {
        return list.stream()
                .map(x -> x * x + 10)
                .filter(x -> x % 10 == 5 || x % 10 == 6)
                .toList();
    }
}
