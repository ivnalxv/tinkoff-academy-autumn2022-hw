package java.task03;

import java.util.Arrays;

public class ConcurrentSequencePrinter {
    private final LockInteger lock;

    private static Runnable createWorker(int number, int length, int repeat, LockInteger lock) {
        return () -> {
            for (int i = 0; i < repeat; i++) {
                try {
                    synchronized (lock) {
                        while (number != lock.data) {
                            lock.wait();
                        }
                        System.out.print(number + 1);
                        lock.data = (lock.data + 1) % length;
                        lock.notifyAll();
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        };
    }

    ConcurrentSequencePrinter() {
        lock = new LockInteger();
    }

    public void start(int length, int repeat) {
        Thread[] threads = new Thread[length];
        for (int i = 0; i < length; i++) {
            threads[i] = new Thread(createWorker(i, length, repeat, lock));
        }

        Arrays.stream(threads).forEach(Thread::start);

        for (int i = 0; i < length; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static class LockInteger {
        public int data = 0;
    }
}